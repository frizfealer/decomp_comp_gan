import numpy as np

import os
import urllib
import gzip
import cPickle as pickle
import gzip

def unpickle(file):
    fo = gzip.open(file, 'rb')
    images, labels = pickle.load(fo)
    fo.close()
    return images, labels

def read_all_data(filenames, data_dir):
    all_data = []
    all_labels = []
    for filename in filenames:
        tmp = unpickle(data_dir + '/' + filename)
        all_data.append(tmp[0])
        all_labels.append(tmp[1])
    images = np.concatenate(all_data, axis=0)
    labels = np.concatenate(all_labels, axis=0)
    return images, labels

def cifar_generator(data, batch_size):
    images, labels = data
    def get_epoch():
        rng_state = np.random.get_state()
        np.random.shuffle(images)
        np.random.set_state(rng_state)
        np.random.shuffle(labels)

        for i in xrange(len(images) / batch_size):
            yield np.copy(images[i*batch_size:(i+1)*batch_size]), np.copy(labels[i*batch_size:(i+1)*batch_size])

    return get_epoch


def load(batch_size, data_dir):
    #train_data = read_all_data(['SVHN_e_train_0','SVHN_e_train_1','SVHN_e_train_2','SVHN_e_train_3'], data_dir)
    #test_data = read_all_data(['SVHN_e_test_0'], data_dir)
    train_data = read_all_data(['SVHN_e_train_0.p'], data_dir)
    test_data = read_all_data(['SVHN_e_train_1.p'], data_dir)
    tmp = np.bincount(train_data[1])
    train_dist = tmp/float(len(train_data[1]))
    return (
        train_dist,
        cifar_generator(train_data, batch_size),
        cifar_generator(test_data, batch_size)
    )

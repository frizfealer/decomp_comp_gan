import numpy

import os
import urllib
import gzip

def mnist_generator(data, batch_size, n_labelled, limit=None):
    images, targets = data

    rng_state = numpy.random.get_state()
    numpy.random.shuffle(images)
    numpy.random.set_state(rng_state)
    numpy.random.shuffle(targets)
    if limit is not None:
        print "WARNING ONLY FIRST {} MNIST DIGITS".format(limit)
        images = images.astype('float32')[:limit]
        targets = targets.astype('int32')[:limit]
    if n_labelled is not None:
        labelled = numpy.zeros(len(images), dtype='int32')
        labelled[:n_labelled] = 1

    def get_epoch():
        rng_state = numpy.random.get_state()
        numpy.random.shuffle(images)
        numpy.random.set_state(rng_state)
        numpy.random.shuffle(targets)

        if n_labelled is not None:
            numpy.random.set_state(rng_state)
            numpy.random.shuffle(labelled)

        image_batches = images.reshape(-1, batch_size, 1024)
        target_batches = targets.reshape(-1, batch_size)

        if n_labelled is not None:
            labelled_batches = labelled.reshape(-1, batch_size)

            for i in xrange(len(image_batches)):
                yield (numpy.copy(image_batches[i]), numpy.copy(target_batches[i]), numpy.copy(labelled))

        else:

            for i in xrange(len(image_batches)):
                yield (numpy.copy(image_batches[i]), numpy.copy(target_batches[i]))

    return get_epoch

def load(batch_size, test_batch_size, n_labelled=None):
    filepath = '/tmp/toy_box_updated4_g_07.npz'
    url = 'http://www.cs.unc.edu/~ycharn/data/toy_box_updated4_g_07.npz'

    if not os.path.isfile(filepath):
        print "Couldn't find toyBox dataset in /tmp, downloading..."
        urllib.urlretrieve(url, filepath)

    tmp = numpy.load('/tmp/toy_box_updated4_g_07.npz')
    data = tmp['arr_0']
    label = tmp['arr_1']
    numpy.random.seed(9999)
    r_idx = numpy.random.permutation(data.shape[0])
    data = data[r_idx]
    label = label[r_idx]
    train_data = (data[:40000], label[:40000])
    dev_data = (data[40000:50000], label[40000:50000])
    test_data = (data[50000:], label[50000:])
    tmp = numpy.bincount(label[:40000])
    train_lab_dist = tmp / 40000. 

    return (
        train_lab_dist,
        mnist_generator(train_data, batch_size, n_labelled),  #50000
        mnist_generator(dev_data, test_batch_size, n_labelled), #10000
        mnist_generator(test_data, test_batch_size, n_labelled) #10000
    )

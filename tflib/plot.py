import numpy as np

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import collections
import time
import cPickle as pickle
import os

_since_beginning = collections.defaultdict(lambda: {})
_since_last_flush = collections.defaultdict(lambda: {})

_iter = [0]
def tick():
	_iter[0] += 1

def plot(name, value):
	_since_last_flush[name][_iter[0]] = value

def flush():
	prints = []
	plt.figure(1)
	for name, vals in _since_last_flush.items():
		prints.append("{}\t{}".format(name, np.mean(vals.values())))
		_since_beginning[name].update(vals)
		x_vals = np.sort(_since_beginning[name].keys())
		y_vals = [_since_beginning[name][x] for x in x_vals]
		plt.figure()
		plt.clf()
		plt.plot(x_vals, y_vals)
		plt.xlabel('iteration')
		plt.ylabel('value')
		plt.title(os.path.basename(name))
		c_dir = os.path.dirname(name)
		plt.savefig(name.replace(' ', '_')+'.jpg')
		plt.close()
    
	all_names = [os.path.basename(name) for name, _ in _since_last_flush.items()]
	plt.figure(1001)
	plt.legend(all_names)
	plt.savefig(os.path.join(c_dir, 'allfig.jpg'))
	plt.close()
	print "iter {}\t{}".format(_iter[0], "\t".join(prints))
	_since_last_flush.clear()

	with open(os.path.join(c_dir, 'log.pkl'), 'wb') as f:
		pickle.dump(dict(_since_beginning), f, pickle.HIGHEST_PROTOCOL)

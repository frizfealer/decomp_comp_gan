import numpy as np
import scipy.misc
import time
import pickle

def make_generator(path, n_files, batch_size, image_dict, start_idx = 0):
    epoch_count = [1]
    def get_epoch():
        images = np.zeros((batch_size, 3, 64, 64), dtype='int32')
        labels = np.zeros((batch_size, 3), dtype='int32')
        files = range(n_files)
        random_state = np.random.RandomState(epoch_count[0])
        random_state.shuffle(files)
        epoch_count[0] += 1
        for n, i in enumerate(files):
            file_name = '{}.png'.format( str(i+start_idx+1).zfill(len(str(len(image_dict)))) )
            #print(file_name)
            #print(image_dict[file_name])
            image = scipy.misc.imread("{}/{}.png".format(path, str(i+start_idx+1).zfill(len(str(len(image_dict)))))) #not sure why scipy read reverse
            images[n % batch_size] = image.transpose(2,0,1)
            labels[n % batch_size] = image_dict[file_name]
            if n > 0 and (n+1) % batch_size == 0:
                yield (images, labels)
    return get_epoch

def load(batch_size, data_dir='/home/ycharn/data/CelebA64'):
    with open( data_dir + '/metadata.pkl', 'rb') as f:
        train_dist, image_dict = pickle.load(f)
    file_num = len(image_dict)
    return (
        train_dist,
        make_generator(data_dir+'/train', 182637, batch_size, image_dict),
        make_generator(data_dir+'/valid', 19962, batch_size, image_dict, start_idx=182637)
    )

if __name__ == '__main__':
    train_gen, valid_gen = load(64)
    t0 = time.time()
    for i, batch in enumerate(train_gen(), start=1):
        print "{}\t{}".format(str(time.time() - t0), batch[0][0,0,0,0])
        if i == 1000:
            break
        t0 = time.time()
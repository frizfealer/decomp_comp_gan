import numpy

import os
import urllib
import gzip
import cPickle as pickle

def mnist_generator(data, batch_size, n_labelled, limit=None):
    images, bgs, fgs, targets = data

    rng_state = numpy.random.get_state()
    numpy.random.shuffle(images)
    numpy.random.set_state(rng_state)
    numpy.random.shuffle(targets)
    numpy.random.set_state(rng_state)
    numpy.random.shuffle(bgs)
    numpy.random.set_state(rng_state)
    numpy.random.shuffle(fgs)
    if limit is not None:
        print "WARNING ONLY FIRST {} MNIST DIGITS".format(limit)
        images = images.astype('float32')[:limit]
        targets = targets.astype('int32')[:limit]
    if n_labelled is not None:
        labelled = numpy.zeros(len(images), dtype='int32')
        labelled[:n_labelled] = 1

    def get_epoch():
        rng_state = numpy.random.get_state()
        numpy.random.shuffle(images)
        numpy.random.set_state(rng_state)
        numpy.random.shuffle(targets)
        numpy.random.set_state(rng_state)
        numpy.random.shuffle(bgs)
        numpy.random.set_state(rng_state)
        numpy.random.shuffle(fgs)

        if n_labelled is not None:
            numpy.random.set_state(rng_state)
            numpy.random.shuffle(labelled)

        image_batches = images.reshape(-1, batch_size, 784)
        target_batches = targets.reshape(-1, batch_size)
        bgs_batches = bgs.reshape(-1, batch_size, 784)
        fgs_batches = fgs.reshape(-1, batch_size, 784)

        if n_labelled is not None:
            labelled_batches = labelled.reshape(-1, batch_size)

            for i in xrange(len(image_batches)):
                yield (numpy.copy(image_batches[i]), numpy.copy(target_batches[i]), numpy.copy(labelled))

        else:

            for i in xrange(len(image_batches)):
                yield (numpy.copy(image_batches[i]), numpy.copy(target_batches[i]), numpy.copy(bgs_batches[i]), numpy.copy(fgs_batches[i]))

    return get_epoch

def load(batch_size, test_batch_size, n_labelled=None):
    filepath = '/tmp/mnist_background.pkl.gz'
    url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background.pkl.gz'

    if not os.path.isfile(filepath):
        print "Couldn't find MNIST-background dataset in /tmp, downloading..."
        urllib.urlretrieve(url, filepath)

    with gzip.open('/tmp/mnist_background.pkl.gz', 'rb') as f:
        train_data, dev_data, test_data = pickle.load(f)
    tmp = numpy.bincount(train_data[3])
    train_lab_dist = numpy.bincount(train_data[3])/float(train_data[3].shape[0])
    return (
        train_lab_dist,
        mnist_generator(train_data, batch_size, n_labelled),  #50000
        mnist_generator(dev_data, test_batch_size, n_labelled), #10000
        mnist_generator(test_data, test_batch_size, n_labelled) #10000
    )
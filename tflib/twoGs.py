import numpy as np

SAMPLE_SIZE = 60000

def generate_data():
	grp1_samples = np.random.multivariate_normal([2,2],[[1, 0],[0, 1]], SAMPLE_SIZE/2)
	grp2_samples = np.random.multivariate_normal([10,2],[[1, 0],[0, 1]], SAMPLE_SIZE/2)
	samples = np.concatenate([grp1_samples, grp2_samples], 0)
	labels = np.zeros((SAMPLE_SIZE), dtype=np.int32)
	labels[SAMPLE_SIZE/2:] = 1
	idx = np.arange(SAMPLE_SIZE)
	np.random.shuffle(idx)
	samples = samples[idx]
	labels = labels[idx]
	return samples, labels

def data_generator(data, batch_size):
    images, labels = data
    def get_epoch():
        rng_state = np.random.get_state()
        np.random.shuffle(images)
        np.random.set_state(rng_state)
        np.random.shuffle(labels)

        for i in xrange(len(images) / batch_size):
            yield np.copy(images[i*batch_size:(i+1)*batch_size]), np.copy(labels[i*batch_size:(i+1)*batch_size])

    return get_epoch

def load(batch_size):
    samples, labels = generate_data()
    train_data = (samples[:50000], labels[:50000])
    test_data = (samples[50000:], labels[50000:])
    tmp = np.bincount(train_data[1])
    train_dist = tmp/float(len(train_data[1]))
    return (
        train_dist,
        data_generator(train_data, batch_size),
        data_generator(test_data, batch_size)
    )


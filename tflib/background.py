import numpy

import os
import urllib
import gzip
import cPickle as pickle

def background_generator(data, batch_size):
    images = data

    rng_state = numpy.random.get_state()
    numpy.random.shuffle(images)
    numpy.random.set_state(rng_state)
    #numpy.random.shuffle(targets)

    def get_epoch():
        rng_state = numpy.random.get_state()
        numpy.random.shuffle(images)
        numpy.random.set_state(rng_state)
        #numpy.random.shuffle(targets)

        image_batches = images.reshape(-1, batch_size, 784)
        #target_batches = targets.reshape(-1, batch_size)
        for i in xrange(len(image_batches)):
            yield numpy.copy(image_batches[i])

    return get_epoch

def load(batch_size, test_batch_size, n_labelled=None):
    filepath = '/tmp/backgrounds.pkl.gz'
    url = 'http://cs.unc.edu/~ycharn/data/backgrounds.pkl.gz'

    if not os.path.isfile(filepath):
        print "Couldn't find background dataset in /tmp, downloading..."
        urllib.urlretrieve(url, filepath)

    with gzip.open('/tmp/backgrounds.pkl.gz', 'rb') as f:
        train_data, dev_data, test_data = pickle.load(f)

    return (
        background_generator(train_data, batch_size),  #50000
        background_generator(dev_data, test_batch_size), #10000
        background_generator(test_data, test_batch_size) #10000
    )
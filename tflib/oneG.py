import numpy as np

def generate_data(sample_size, mean_vec, cov_mat):
    if mean_vec == None:
        mean_vec = [0, 0]
    if cov_mat == None:
        cov_mat = [[1, 0],[0, 1]]
    samples = np.random.multivariate_normal(mean_vec, cov_mat, sample_size)
    return samples

def data_generator(data, batch_size):
    images = data
    def get_epoch():
        rng_state = np.random.get_state()
        np.random.shuffle(images)
        np.random.set_state(rng_state)

        batch_num = len(images) / batch_size
        if len(images) % batch_size != 0:
            batch_num+=1

        for i in xrange(batch_num):
            if i < batch_num - 1:
                yield np.copy(images[i*batch_size:(i+1)*batch_size])
            else:
                yield np.copy(images[i*batch_size:])

    return get_epoch

def load(batch_size, sample_size = 60000, mean_vec = None, cov_mat = None):
    samples = generate_data(sample_size, mean_vec, cov_mat)
    train_size = int(sample_size*.8)
    train_data = samples[:train_size]
    test_data = samples[train_size:]
    return (
        data_generator(train_data, batch_size),
        data_generator(test_data, batch_size)
    )

